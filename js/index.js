(function ($) {
  "use strict";

  var foodsPage = 1;

  function foodsPageShow(page) {
    page = page || 0;
    var pageSize = 8;

    var htmls = "";
    for (var i = 0; i < pageSize; i++) {
      var index = i + page * pageSize + 1;

      htmls += '<div class="col-6 col-sm-6 col-md-6 col-lg-3 wow fadeIn" data-wow-delay="0.' + i + 1 + 's">';
      htmls += '<div class="gallery-item text-center overflow-hidden">';
      htmls += '<div class="overflow-hidden gallery-item-img">';
      htmls += '<img class="img-fluid" src="img/foods/' + index + '.jpg" alt="" />';
      htmls += "</div>";
      htmls += "</div>";
      htmls += "</div>";
    }

    $("#foods").html(htmls);
  }

  setInterval(
    function () {
      foodsPage++;
      if (foodsPage >= 8) {
        foodsPage = 0;
      }
      foodsPageShow(foodsPage);
    },

    8000
  );
})(jQuery);
